

==================================== POKEMON PROJECT =============================================


1. Only displaying pokemon from generation 1
2. You can try the apk release version by installing app-release.apk




 
HOW TO USE :
1. Catch pokemon from 5 randomized list, by tapping the pokeball icon.
2. Shuffle the list by tapping Keep Exploring.
3. One you've caught a pokemon, they will appear in your bag.
4. You can release the pokemon you've caught by tapping the trash icon.
5. You can see their details by tapping the pokemon in your bag.
6. You can search pokemon by their name, but however there is a bug that the pokemon image displayed on the search screen isn't correct.








Regards,



Naufal
