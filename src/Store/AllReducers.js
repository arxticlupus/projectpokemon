import {combineReducers} from 'redux';

//reducers
import GlobalReducer from './GlobalReducer';
import HomeReducer from '../Screens/Home/Redux/reducer';
import BagReducer from '../Screens/Bag/Redux/reducer';
import PokemonDetailReducer from '../Screens/PokemonDetails/Redux/reducer';

export const AllReducers = combineReducers({
  Global: GlobalReducer,
  HomeReducer,
  BagReducer,
  PokemonDetailReducer,
});
