import {all} from 'redux-saga/effects';
//screen saga
import sagaHome from '../Screens/Home/Redux/sagaHome';
import sagaDetail from '../Screens/PokemonDetails/Redux/sagaPokemonDetail';

function* SagaWatcher() {
  yield all([sagaHome(), sagaDetail()]);
}

export default SagaWatcher;
