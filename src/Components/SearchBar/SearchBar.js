import React from 'react';
import {StyleSheet, TouchableOpacity, TextInput} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

const SearchBar = props => {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <TextInput
        editable={props.editable}
        onChangeText={props.onChangeText}
        style={styles.searchBar}
        value={props.value}
        placeholder="Search Pokemon"
        placeholderTextColor="#CCCED0"
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  searchBar: {
    fontSize: 24,
    width: '100%',
    height: moderateScale(50),
    backgroundColor: '#F6F6F6',
    borderColor: '#EAEAEA',
    borderWidth: 2,
    color: 'black',
    borderRadius: 10,
  },
});

export default SearchBar;
