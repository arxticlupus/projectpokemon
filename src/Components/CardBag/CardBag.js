import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

const CardBag = props => {
  return (
    <TouchableOpacity onPress={props.onPressCard}>
      <View style={styles.container} key={props.key}>
        <Text style={styles.textName}>{props.title}</Text>
        <Image
          source={{uri: props.image}}
          style={styles.image}
          resizeMode="contain"
        />
        <TouchableOpacity onPress={props.onPress}>
          <Image
            source={require('../../Assets/Icons/trash.png')}
            style={styles.imagePokeball}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderColor: '#EAEAEA',
    borderWidth: 2,
    height: moderateScale(160),
    width: moderateScale(120),
    marginHorizontal: moderateScale(20),
    marginTop: moderateScale(20),
    borderRadius: 10,
  },
  image: {
    height: moderateScale(80),
    width: moderateScale(80),
    alignSelf: 'center',
  },
  imagePokeball: {
    height: moderateScale(15),
    width: moderateScale(15),
    alignSelf: 'center',
  },
  textName: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: moderateScale(20),
  },
});

export default CardBag;
