import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

const Card = props => {
  return (
    <View style={styles.container} key={props.key}>
      <Text style={styles.textName}>{props.title}</Text>
      <Image
        source={{uri: props.image}}
        style={styles.image}
        resizeMode="contain"
      />
      <TouchableOpacity onPress={props.onPress}>
        <Image
          source={require('../../Assets/Icons/pokeball.png')}
          style={styles.imagePokeball}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderColor: '#EAEAEA',
    borderWidth: 2,
    height: moderateScale(280),
    width: moderateScale(240),
    marginHorizontal: moderateScale(20),
    marginTop: moderateScale(20),
    borderRadius: 10,
  },
  image: {
    height: moderateScale(160),
    width: moderateScale(160),
    alignSelf: 'center',
  },
  imagePokeball: {
    height: moderateScale(30),
    width: moderateScale(30),
    alignSelf: 'center',
  },
  textName: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: moderateScale(20),
  },
});

export default Card;
