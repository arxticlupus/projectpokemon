import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {actionLoading} from '../../../Store/GlobalAction';

//import action
import {setHome} from './action';
import {GET_HOME} from './action';

function* getPokemonList(action) {
  console.log(action.payload);

  yield put(actionLoading(true));
  try {
    const result = yield axios.get(
      'https://pokeapi.co/api/v2/pokemon?limit=151',
    );

    if (result) {
      yield put(setHome(result.data.results));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(actionLoading(false));
  }
}

function* sagaHome() {
  yield takeLatest(GET_HOME, getPokemonList);
}

export default sagaHome;
