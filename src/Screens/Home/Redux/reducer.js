import {SET_HOME} from './action';

const initialState = {
  data: [],
};

const HomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_HOME:
      return {
        ...state,
        data: action.payload,
      };

    default:
      return state;
  }
};

export default HomeReducer;
