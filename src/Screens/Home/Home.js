import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  ImageBackground,
  View,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {useDispatch, useSelector} from 'react-redux';
import Card from '../../Components/Card/Card';
import {getHome} from './Redux/action';
import {setBag} from '../Bag/Redux/action';
import SearchBar from '../../Components/SearchBar/SearchBar';

const Home = props => {
  const listData = useSelector(state => state.HomeReducer?.data);
  const listBag = useSelector(state => state.BagReducer?.data);
  const isLoading = useSelector(state => state.Global?.loading);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getHome());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const BottomComponent = () => {
    return (
      <View style={styles.bottomTextContainer}>
        <Text style={styles.bottomText}>
          Don't see the pokemon you're looking for?
        </Text>
        <TouchableOpacity
          onPress={() => {
            dispatch(getHome());
          }}>
          <Text style={styles.bottomTextLink}> Keep exploring!</Text>
        </TouchableOpacity>
      </View>
    );
  };

  if (isLoading) {
    return (
      <View style={styles.container}>
        <View style={{marginTop: moderateScale(250)}}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.childContainer}>
          {/* Header */}
          <Text style={styles.text}>What Pokemon are you looking for?</Text>
          {/* Search Bar Component */}
          <SearchBar
            editable={false}
            onPress={() => {
              props.navigation.navigate('Search');
            }}
          />
          {/* Card List */}
          <View style={styles.topFiveText}>
            <Text style={styles.textSearchBar}>
              5 Wild Pokemon has appeared!
            </Text>
          </View>
          <ScrollView horizontal>
            {listData
              .map((item, index) => {
                const pokemonIndex = index + 1;
                const imageLink = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonIndex}.png`;
                return (
                  <View key={index}>
                    <Card
                      title={item.name}
                      image={imageLink}
                      onPress={() => {
                        if (listBag.length < 10) {
                          dispatch(
                            setBag({
                              name: item.name,
                              image: imageLink,
                            }),
                          );
                          ToastAndroid.show(
                            `Gotcha! ${item.name} was caught! But the other Pokemon fled.`,
                            ToastAndroid.SHORT,
                          );
                        } else {
                          ToastAndroid.show(
                            'Your bag is full!',
                            ToastAndroid.SHORT,
                          );
                        }
                      }}
                    />
                  </View>
                );
              })
              .sort(() => Math.random() - 0.5)
              .slice(0, 5)}
          </ScrollView>
          <BottomComponent />
        </View>
      </ScrollView>
      <ImageBackground
        source={require('../../Assets/Images/main_background.png')}
        resizeMode="cover"
        style={[styles.fixed, styles.containter]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  childContainer: {
    marginHorizontal: moderateScale(16),
    marginTop: moderateScale(20),
  },
  image: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
  text: {
    color: 'black',
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: moderateScale(30),
  },
  textSearchBar: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  fixed: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1,
  },
  topFiveText: {
    marginTop: moderateScale(20),
  },
  bottomTextContainer: {
    marginTop: moderateScale(60),
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  bottomText: {
    fontSize: 12,
    color: 'black',
  },
  bottomTextLink: {
    fontSize: 12,
    color: '#9EA5CA',
  },
});

export default Home;
