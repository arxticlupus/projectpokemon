import React from 'react';
import {StyleSheet, Text, View, Image, ActivityIndicator} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getDetail} from './Redux/action';
import {moderateScale} from 'react-native-size-matters';

const PokemonDetails = (props, route) => {
  const name = props.route.params.pokemonName;
  const dispatch = useDispatch();
  const detailData = useSelector(state => state.PokemonDetailReducer?.data);
  const isLoading = useSelector(state => state.Global?.loading);
  const [loading, setLoading] = React.useState(true);
  const pokemonTypes = useSelector(
    state => state.PokemonDetailReducer?.data?.types,
  );
  React.useEffect(() => {
    dispatch(getDetail(name));
    setTimeout(() => {
      setLoading(false);
    }, 3000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const Header = () => {
    return (
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>{detailData.name}</Text>
        <Text style={styles.headerText}>
          #0{detailData.game_indices[0].game_index}
        </Text>
      </View>
    );
  };

  const ImageSection = () => {
    return (
      <View style={styles.imageContainer}>
        <Image
          source={{uri: detailData?.sprites?.front_default}}
          style={styles.image}
          resizeMode="contain"
        />
        <Image
          source={{uri: detailData?.sprites?.front_shiny}}
          style={styles.image}
          resizeMode="contain"
        />
      </View>
    );
  };

  const TypeSection = () => {
    return (
      <View style={styles.typeContainer}>
        <Text style={styles.typeTitle}>Type :</Text>
        <View style={styles.type}>
          {pokemonTypes.map((item, index) => {
            return <Text style={styles.typeText}> {item.type.name} </Text>;
          })}
        </View>
      </View>
    );
  };

  if (loading || isLoading) {
    return (
      <View style={styles.container}>
        <View style={{marginTop: moderateScale(250)}}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      </View>
    );
  }
  if (detailData === undefined || detailData.length < 1 || !detailData) {
    return <></>;
  }
  return (
    <View style={styles.container}>
      {/* Header */}
      <View style={styles.childContainer}>
        <Header />
        <ImageSection />
        <TypeSection />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DDE3EA',
  },
  headerContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: moderateScale(100),
    borderRadius: 10,
    justifyContent: 'space-around',
    marginTop: 20,
  },
  typeContainer: {
    backgroundColor: 'white',
    height: moderateScale(100),
    borderRadius: 10,
    marginTop: moderateScale(20),
  },
  headerText: {
    fontSize: 32,
    color: 'black',
    marginTop: moderateScale(24),
  },
  imageText: {
    fontSize: 18,
    color: 'black',
    marginTop: moderateScale(24),
    alignItems: 'center',
  },
  imageContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: moderateScale(320),
    borderRadius: 10,
    justifyContent: 'space-around',
    marginTop: 20,
  },
  childContainer: {
    marginHorizontal: moderateScale(16),
    marginTop: moderateScale(20),
  },
  image: {
    height: moderateScale(160),
    width: moderateScale(160),
    alignSelf: 'center',
  },
  typeTitle: {
    color: 'black',
    textAlign: 'center',
    marginTop: moderateScale(20),
    fontSize: 12,
  },
  type: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginTop: moderateScale(10),
  },
  typeText: {
    color: 'black',
    fontSize: 18,
  },
});

export default PokemonDetails;
