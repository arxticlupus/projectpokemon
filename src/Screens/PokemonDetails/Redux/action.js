export const SET_DETAIL = 'SET_DETAIL';
export const GET_DETAIL = 'GET_DETAIL';

export const setDetail = payload => {
  return {
    type: SET_DETAIL,
    payload,
  };
};

export const getDetail = payload => {
  return {
    type: GET_DETAIL,
    payload,
  };
};
