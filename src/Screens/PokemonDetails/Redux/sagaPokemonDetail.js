import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {actionLoading} from '../../../Store/GlobalAction';

//import action
import {setDetail} from './action';
import {GET_DETAIL} from './action';

function* getPokemonDetails(action) {
  yield put(actionLoading(true));
  try {
    const result = yield axios.get(
      `https://pokeapi.co/api/v2/pokemon/${action.payload}`,
    );

    if (result) {
      yield put(setDetail(result.data));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(actionLoading(false));
  }
}

function* sagaDetail() {
  yield takeLatest(GET_DETAIL, getPokemonDetails);
}

export default sagaDetail;
