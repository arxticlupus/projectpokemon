import {SET_BAG} from './action';

const initialState = {
  data: [],
};

const BagReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_BAG:
      return {
        ...state,
        data: [...state.data, action.payload],
      };

    default:
      return state;
  }
};

export default BagReducer;
