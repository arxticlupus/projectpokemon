export const SET_BAG = 'SET_BAG';

export const setBag = payload => {
  return {
    type: SET_BAG,
    payload,
  };
};
