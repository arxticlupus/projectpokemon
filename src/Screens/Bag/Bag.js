import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ActivityIndicator,
  ScrollView,
  Dimensions,
  ToastAndroid,
} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import CardBag from '../../Components/CardBag/CardBag';

const Bag = props => {
  const listBag = useSelector(state => state.BagReducer?.data);
  const [loading, setLoading] = React.useState(true);
  const isFocused = useIsFocused();

  React.useEffect(() => {
    if (isFocused) {
      setTimeout(() => {
        setLoading(false);
      }, 2000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) {
    return (
      <View style={styles.container}>
        <View style={{marginTop: moderateScale(250)}}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      </View>
    );
  }
  if (listBag === undefined || listBag.length < 1 || listBag === []) {
    <View style={styles.container}>
      <Text style={styles.text}>Tidak ada bag</Text>
    </View>;
  }
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.childContainer}>
          <Text style={styles.text}>Your Bag</Text>
          <View style={styles.cardContainer}>
            {listBag
              .map((item, index) => {
                return (
                  <CardBag
                    key={index}
                    title={item.name}
                    image={item.image}
                    onPressCard={() => {
                      props.navigation.navigate('Detail', {
                        pokemonName: item.name,
                      });
                    }}
                    onPress={() => {
                      listBag.splice(index, 1);
                      ToastAndroid.show(
                        `Bye ${item.name}!`,
                        ToastAndroid.SHORT,
                      );
                      setLoading(true);
                      setTimeout(() => {
                        setLoading(false);
                      }, 1000);
                    }}
                  />
                );
              })
              .splice(0, 10)}
          </View>
        </View>
      </ScrollView>
      <ImageBackground
        source={require('../../Assets/Images/main_background.png')}
        resizeMode="cover"
        style={[styles.fixed, styles.imageContainer]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  childContainer: {
    marginHorizontal: moderateScale(16),
    marginTop: moderateScale(20),
  },
  text: {
    color: 'black',
    fontSize: 32,
    fontWeight: 'bold',
  },
  bgImage: {
    flex: 1,
  },
  fixed: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1,
  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginBottom: 20,
  },
});

export default Bag;
