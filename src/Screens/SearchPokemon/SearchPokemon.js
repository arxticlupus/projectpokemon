import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  StyleSheet,
  View,
  FlatList,
  ImageBackground,
  ToastAndroid,
} from 'react-native';
import SearchBar from '../../Components/SearchBar/SearchBar';
import Card from '../../Components/Card/Card';
import {setBag} from '../Bag/Redux/action';
import {moderateScale} from 'react-native-size-matters';

const SearchPokemon = () => {
  const dispatch = useDispatch();
  const [search, setSearch] = useState('');

  const [filteredDataSource, setFilteredDataSource] = useState([]);

  const masterDataSource = useSelector(state => state.HomeReducer?.data);
  const listBag = useSelector(state => state.BagReducer?.data);

  useEffect(() => {
    setFilteredDataSource(masterDataSource);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const searchFilterFunction = text => {
    if (text) {
      const newData = masterDataSource.filter(function (item) {
        const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();

        const textData = text.toUpperCase();

        return itemData.indexOf(textData) > -1;
      });

      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };

  const ItemView = ({item, index}) => {
    const pokemonIndex = index + 1;
    const imageLink = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonIndex}.png`;
    if (search === '') {
      return <></>;
    }
    return (
      <Card
        title={item.name}
        image={imageLink}
        onPress={() => {
          if (listBag.length < 10) {
            dispatch(
              setBag({
                name: item.name,
                image: imageLink,
              }),
            );
            ToastAndroid.show(
              `Gotcha! ${item.name} was caught! But the other Pokemon fled`,
              ToastAndroid.SHORT,
            );
          } else {
            ToastAndroid.show('Your bag is full!', ToastAndroid.SHORT);
          }
        }}
      />
    );
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../Assets/Images/main_background.png')}
        resizeMode="cover"
        style={styles.bgImage}>
        <View style={styles.childContainer}>
          <FlatList
            ListHeaderComponent={
              <SearchBar
                onChangeText={text => searchFilterFunction(text)}
                value={search}
              />
            }
            data={filteredDataSource}
            keyExtractor={(item, index) => index.toString()}
            renderItem={ItemView}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  childContainer: {
    marginHorizontal: moderateScale(16),
    marginBottom: moderateScale(12),
  },
  itemStyle: {
    padding: moderateScale(10),
    color: 'black',
  },
  bgImage: {
    flex: 1,
  },
});

export default SearchPokemon;
