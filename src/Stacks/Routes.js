import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import BottomTabs from './BottomTabs';
import PokemonDetails from '../Screens/PokemonDetails/PokemonDetails';
import SearchPokemon from '../Screens/SearchPokemon/SearchPokemon';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <Stack.Navigator initialRouteName={'Main'}>
      <Stack.Screen
        name="Main"
        component={BottomTabs}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Detail" component={PokemonDetails} />
      <Stack.Screen name="Search" component={SearchPokemon} />
    </Stack.Navigator>
  );
};

export default Routes;
