import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Bag from '../Screens/Bag/Bag';
import Home from '../Screens/Home/Home';

const Tab = createBottomTabNavigator();

const BottomTabs = () => {
  return (
    <Tab.Navigator initialRouteName="Home" screenOptions={{headerShown: false}}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Bag" component={Bag} />
    </Tab.Navigator>
  );
};

export default BottomTabs;
